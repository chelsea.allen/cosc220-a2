package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    /*
     * Test if boxComplete returns true for complete box
     */
    @Test
    public void testBoxCompleteDetectsCompletedBoxes() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(15, 8, 2);
        grid.drawHorizontal(0,0,1);
        grid.drawHorizontal(0,1,1);
        grid.drawVertical(0,0,1);
        grid.drawVertical(1,0,1);
        assertTrue(grid.boxComplete(0, 0));
    }

    /*
     * 3 tests to determine if boxComplete returns false for incomplete box (no lines, 1 line, 2 lines)
     */
    @Test
    public void testBoxCompleteNoLinesDrawn() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(20, 10, 2);
        assertFalse(grid.boxComplete(0, 0));
    }

    @Test
    public void testBoxCompleteOneLineDrawn() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(20, 10, 2);
        grid.drawHorizontal(0, 0, 1);
        assertFalse(grid.boxComplete(0, 0));
    }

    @Test
    public void testBoxCompleteTwoLinesDrawn() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(20, 10, 2);
        grid.drawHorizontal(0, 0, 1);
        grid.drawVertical(0, 0, 1);
        assertFalse(grid.boxComplete(0, 0));
    }

    /*
     * Test if horizontal draw method throws exception for already-drawn lines
     */
    @Test
    public void testDrawHorizontalDetectsRedrawnLines() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(20, 10, 2);

        // draw a horizontal line at (0, 0) and claim a box
        grid.drawHorizontal(0, 0, 1);

        // attempt to draw the same line, should throw an IllegalStateException
        assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0, 0, 1));

    }

    /*
     * Test if vertical draw method throws exception for already-drawn lines
     */
    @Test
    public void testDrawVerticalDetectsRedrawnLines() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(20, 10, 2);

        // draw a vertical line at (0, 0) and claim a box
        grid.drawVertical(0, 0, 1);

        // attempt to draw the same line, should throw an IllegalStateException
        assertThrows(IllegalStateException.class, () -> grid.drawVertical(0, 0, 1));

    }
}
